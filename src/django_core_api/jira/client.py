
from dateutil.relativedelta import relativedelta
from django.conf import settings

import datetime
import requests
import urllib3
import os

urllib3.disable_warnings()

class JiraClient(object):

    def __init__(self):
        self.username = settings.JIRA_SERVER['username']
        self.password = settings.JIRA_SERVER['password']
        self.host = settings.JIRA_SERVER['host']
        self.issue_endpoint = settings.JIRA_SERVER['issue_endpoint']
        self.search_endpoint = settings.JIRA_SERVER['search_endpoint']
        self.fields = settings.JIRA_SERVER['fields']

        self.proxies = None        
        if settings.PROXY['use_proxy']:
            self.proxies = {
                'http': settings.PROXY['http'],
                'https': settings.PROXY['https'],
            }

    def search(self, jql):
        endpoint = "%s/%s?jql=%s&fields=%s&maxResults=1000" % (self.host, self.search_endpoint, jql, self.fields)
        r = requests.get(
            endpoint,
            auth=(self.username, self.password),
            headers={
                "Content-Type": "application/json"
            },
            proxies=self.proxies,
            verify=False
        )
        if r.status_code == 200:
            return r.json()
        raise Exception("Not possible search on Jira. \nendpoint: %s \nstatus_code: %s \nresponde: %s" % (endpoint, r.status_code, r.text))

    def get_stories_done(self, project):
        start_date = datetime.datetime(2019, 1, 1, 0, 0, 0, 0)
        today = datetime.datetime.now()
        data = []
        while start_date < today:
            end_date = start_date
            end_date = end_date + relativedelta(months=1)
            end_date = end_date - relativedelta(days=1)

            jql = "project = %s AND issuetype not in (epic, Sub-tarefa) AND created >= '%s' AND created <= '%s' AND (resolutiondate IS EMPTY or resolutiondate >= '2019/01/01') ORDER BY Rank ASC" % (project, start_date.strftime("%Y/%m/%d"), end_date.strftime("%Y/%m/%d"))

            issues = self.search(jql)
            for issue in issues['issues']:
                resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
                month_resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
                if month_resolutiondate is not None:
                    month_resolutiondate = month_resolutiondate.replace(day=1, hour=0, minute=0, second=0)
                item = {
                    "key": issue['key'],
                    "type": issue['fields']['issuetype']['name'],
                    "summary": issue['fields']['summary'],
                    "team": project if issue['fields']['customfield_14926'] is None else issue['fields']['customfield_14926']['name'],
                    "duedate": issue['fields']['duedate'],
                    "resolutiondate": resolutiondate if resolutiondate != None else None,
                    "status": issue['fields']['status']['name'],
                    "epic": issue['fields']['customfield_12602'],
                    "rank": issue['fields']['customfield_12605'],
                    "sprint": issue['fields']['customfield_12600'],
                    "month_resolutiondate": month_resolutiondate if month_resolutiondate != None else None,
                }
                data.append(item)

            start_date = start_date + relativedelta(months=1)
            end_date = end_date + relativedelta(months=1)

        return data
