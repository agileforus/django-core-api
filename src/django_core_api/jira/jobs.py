from django_core_api.jira.client import JiraClient
from influxdb import InfluxDBClient
from django.conf import settings
from dateutil.relativedelta import relativedelta

import datetime

class JiraServerJob(object):

    def __init__(self):
        self.jira_client = JiraClient()

        self.influx_client = InfluxDBClient(
            host=settings.INFLUXDB['host'], 
            port=settings.INFLUXDB['port'],
            username=settings.INFLUXDB['username'],
            password=settings.INFLUXDB['password'])
    
        self.influx_client.switch_database(settings.INFLUXDB['database'])


    def calculate_stories_done(self, project):
        self.influx_client.delete_series(measurement="closed_stories", tags={"project": project})
        start_date = datetime.datetime(2019, 1, 1, 0, 0, 0, 0)
        today = datetime.datetime.now()
        while start_date < today:
            end_date = start_date
            end_date = end_date + relativedelta(months=1)
            end_date = end_date - relativedelta(days=1)

            jql = "project = %s AND issuetype not in (epic, Sub-tarefa) AND created >= '%s' AND created <= '%s' AND (resolutiondate >= '2019/01/01') ORDER BY Rank ASC" % (project, start_date.strftime("%Y/%m/%d"), end_date.strftime("%Y/%m/%d"))
            
            data = {}
            issues = self.jira_client.search(jql)
            for issue in issues['issues']:
                item = self.get_item(issue, project)
                if start_date.strftime("%Y-%m-%d") not in data:
                    data[start_date.strftime("%Y-%m-%d")] = {}
                if item["team"] not in data[start_date.strftime("%Y-%m-%d")]:
                    data[start_date.strftime("%Y-%m-%d")][item["team"]] = {
                        'value': 0
                    }
                data[start_date.strftime("%Y-%m-%d")][item["team"]]['value'] = data[start_date.strftime("%Y-%m-%d")][item["team"]]['value'] + 1

            points = []
            for data_month in data:
                for data_project in data[data_month]:
                    points.append({
                        "measurement": "closed_stories",
                        "tags": {
                            "project": data_project,
                        },
                        "time": "%sT00:00:00Z" % data_month,
                        "fields" :{
                            "value": data[data_month][data_project]['value']
                        }
                    })

            print(points)
            self.influx_client.write_points(points)

            start_date = start_date + relativedelta(months=1)
            end_date = end_date + relativedelta(months=1)

        return data
    
    def get_item(self, issue, project):
        resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
        month_resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
        if month_resolutiondate is not None:
            month_resolutiondate = month_resolutiondate.replace(day=1, hour=0, minute=0, second=0)
        return {
                    "key": issue['key'],
                    "type": issue['fields']['issuetype']['name'],
                    "summary": issue['fields']['summary'],
                    "team": project if issue['fields']['customfield_14926'] is None else issue['fields']['customfield_14926']['name'],
                    "duedate": issue['fields']['duedate'],
                    "resolutiondate": resolutiondate if resolutiondate != None else None,
                    "status": issue['fields']['status']['name'],
                    "epic": issue['fields']['customfield_12602'],
                    "rank": issue['fields']['customfield_12605'],
                    "sprint": issue['fields']['customfield_12600'],
                    "month_resolutiondate": month_resolutiondate if month_resolutiondate != None else None,
        }

def jiraserver_stories(project_key):

    jiraserver_job = JiraServerJob()
    jiraserver_job.calculate_stories_done(project_key)

    return None

    client = JiraClient()
    stories = client.get_stories_done(project_key)
    
    data = []
    for story in stories:
        if story['resolutiondate'] is not None:
            item = {
                "measurement": "closed_stories",
                "tags": {
                    "project": project_key,
                    "key": story['key'],
                    "team": story['team'],
                    "type": story['type'],
                },
                "time": story['month_resolutiondate'].strftime("%Y-%m-%dT%H:%M:%SZ"),
                "fields" :{
                    "value": 1
                }
            }
            data.append(item)
    
    influx_client = InfluxDBClient(
        host=settings.INFLUXDB['host'], 
        port=settings.INFLUXDB['port'],
        username=settings.INFLUXDB['username'],
        password=settings.INFLUXDB['password'])
    
    influx_client.switch_database(settings.INFLUXDB['database'])
    
    influx_client.delete_series(measurement="closed_stories", tags={"project": project_key})

    influx_client.write_points(data)

    print(data)