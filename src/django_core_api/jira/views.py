from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from django_core_api.jira import jobs
from datetime import timedelta, datetime

import django_rq

# Create your views here.

class JobsViewSet(viewsets.ViewSet):
    
    def create(self, request):
        django_rq.enqueue(jobs.jiraserver_stories, request.data['params']['project_key'])
        return Response()

class JobsSchedullerViewSet(viewsets.ViewSet):

    def create(self, request):
        scheduler = django_rq.get_scheduler('default')
        # scheduler.enqueue_in(timedelta(seconds=5), jobs.jiraserver_stories, request.data['params'])
        scheduler.schedule(
            scheduled_time=datetime.utcnow(),
            func=jobs.jiraserver_stories,
            args=[request.data['params']],
            interval=5,
        )
        return Response()